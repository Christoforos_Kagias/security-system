/*
 * 	General_Functions.c
 *
 * 	Created on: Mar 10, 2021
 * 	Author: Christofer Kagias
 */

#include "custom_functions.h"
#include "MFRC522 RFID Reader.h"
#include "stm32f1xx.h"
#include "stdlib.h"
#include "usbd_cdc_if.h"

/************************************DO NOT CHANGE ANYTHING BEYOND THIS LINE***************************************************/

/*
 * Color combinations for set_rgb function
 * (255, 0, 0);					blue
 * (0, 255, 0);					yellow
 * (0, 0, 255);					pink
 * (255, 255, 0);				green
 * (0, 255, 255);				red
 * (0, 0, 0);					white (close enough)
 * (255, 255, 255);				off
 */

/*
 * Global declarations
 */
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;

/*
 * Variable declarations
 */

// RFID specific variables
uint8_t passwd[] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};		// Default RFID Tag password to access data
uint8_t status;											// Indicates if accessing the card data was successful
uint8_t authenticate;									// Indicates if the card data is valid
uint8_t str[MAX_LEN]; 									// Max_LEN = 16
uint8_t UID[5];											// RFID UID
uint8_t recv_flag[1];										// Buffer for USB data reception

/*
 * Void function that reads the UID of the MIFARE 1k tag through the RC522 RFID Reader
 * Arguments: none
 */
void readUID()
{
	status = MFRC522_Request(PICC_REQIDL, str);	// Read tag UID
	status = MFRC522_Anticoll(str);// Show tag UID
	if (status == MI_OK)
	{
		memcpy(UID, str, 5);

		checkUID(UID[0], UID[1], UID[2], UID[3], UID[4]);
	}
	MFRC522_Init();
}

/*
 * Void function that handles the serial communication between the MCU and the master console
 * Arguments: 5 uint8_t values that correspond to a part of the UID of the MIFARE 1k tag
 *
 * The MCU transmits the 5 8-bit values to the master console and after a comparison with the existing database, receives through
 * serial communication a value that corresponds to the result of the database search
 *
 * 1 -> UID is not valid 	-> the user is valid     --------------------------------------------------------------> blink green
 * 2 -> UID is not valid 	-> the user is not valid --------------------------------------------------------------> blink red
 *
 */
void checkUID(uint8_t bitset1, uint8_t bitset2, uint8_t bitset3, uint8_t bitset4, uint8_t bitset5)
{
	CDC_Transmit_FS(&bitset1, 1);
	HAL_Delay(10);
	CDC_Transmit_FS(&bitset2, 1);
	HAL_Delay(10);
	CDC_Transmit_FS(&bitset3, 1);
	HAL_Delay(10);
	CDC_Transmit_FS(&bitset4, 1);
	HAL_Delay(10);
	CDC_Transmit_FS(&bitset5, 1);
	HAL_Delay(10);


	if (recv_flag[0] == 1)
	{
		blink(255, 255, 0);
		buzzer(0, 75);
		HAL_Delay(2000);
	}
	else
	{
		blink(0, 255, 255);
		buzzer(0, 500);
		HAL_Delay(2000);
	}
}

/*
 * Void function that handles the external common anode RGB LED with the use of PWM.
 * Arguments: 3 uint32_t values from 0 to 255 (three 255 values turn the LED off)
 */
void set_rgb(uint32_t color1, uint32_t color2, uint32_t color3)
{
	htim2.Instance->CCR1 = color1;
	htim2.Instance->CCR2 = color2;
	htim2.Instance->CCR3 = color3;
}

/*
 * Void function that blinks the external common anode RGB (uses set_rgb() function)
 * Arguments: 3 uint32_t values from 0 to 255 (three 255 values turn the LED off)
 */
void blink(uint32_t color1, uint32_t color2, uint32_t color3)
{
	for (int i = 0; i<3; i++)
	{
		set_rgb(color1, color2, color3);
		HAL_Delay(100);
		set_rgb(255, 255, 255);
		HAL_Delay(100);
	}
}

/*
 * Void function that controls the buzzer
 * Arguments: tone and delay (indicates correct or incorrect output)
 */
void buzzer(uint32_t tone, uint32_t delay)
{
	for (int i = 0; i<3; i++)
	{
		htim3.Instance->CCR1 = tone;
		HAL_Delay(delay);
		htim3.Instance->CCR1 = 255;
		HAL_Delay(delay);
	}
	htim3.Instance->CCR1 = 255;
}
