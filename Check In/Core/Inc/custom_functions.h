/*
 *	General_Functions.h
 *
 * 	Created on: Mar 10, 2021
 * 	Author: Christofer Kagias
 */

#ifndef INC_CUSTOM_FUNCTIONS_H_
#define INC_CUSTOM_FUNCTIONS_H_

#include "MFRC522 RFID Reader.h"
#include "stm32f1xx.h"
#include "string.h"

/*
 * Private function declarations
 */

void readUID();
void checkUID(uint8_t bitset1, uint8_t bitset2, uint8_t bitset3, uint8_t bitset4, uint8_t bitset5);
void set_rgb(uint32_t red, uint32_t green, uint32_t blue);
void blink(uint32_t color1, uint32_t color2, uint32_t color3);
void buzzer(uint32_t tone, uint32_t delay);

#endif /* INC_CUSTOM_FUNCTIONS_H_ */
