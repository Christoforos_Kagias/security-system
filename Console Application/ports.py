import subprocess

def ACM_ports():
    system_port=[]
    x = subprocess.Popen('ls /dev/ttyACM*',
                     shell=True,
                     stdout = subprocess.PIPE,
                     stderr = subprocess.PIPE)
    stdout, stderr = x.communicate()
    ports = stdout.splitlines()
    for i in range(len(ports)):
        system_port.append(ports[i].decode("utf-8"))
    return system_port


def USB_ports():
    system_port=[]
    x = subprocess.Popen('ls /dev/ttyUSB*',
                     shell=True,
                     stdout = subprocess.PIPE,
                     stderr = subprocess.PIPE)
    stdout, stderr = x.communicate()
    ports = stdout.splitlines()
    for i in range(len(ports)):
        system_port.append(ports[i].decode("utf-8"))
    return system_port

def STM_devices():
    x = subprocess.Popen('lsusb',
                     shell=True,
                     stdout = subprocess.PIPE,
                     stderr = subprocess.PIPE)
    stdout, stderr = x.communicate()
    devices = stdout.splitlines()
    stm_dev = []
    for i in range(0,len(devices)):
        temp = devices[i].split()
        if temp[5].decode("utf-8") == "0483:5740" or temp[5].decode("utf-8") == "0483:5741":
            print(temp[5].decode("utf-8"))
            stm_dev.append(temp[5].decode("utf-8"))
        else:
            print("bla")
    return stm_dev