"""
Functions to implement:

Mandatory:
0) Create databases Users.db, Keys.db and Code.db
1) Add new user to the system
2) Remove user from the system
3) Perform check in validation check (thread function)
4) Handle lab key allocation (thread function)
5) Activate the system ---------------> implemented in keypad.py  
6) Deactivate the system -------------> implemented in keypad.py
7) Replace User tag missing
8) Replace User tag missing
9) Keep track of who has got keys to the lab
10) Change user roles (if neccessary)
11) Add a new set of lab keys
12) Remove a set of lab keys
13) Change the arming code

Optional:

a) Temperature sensor
"""

import tkinter as tk
import sqlite3
import RPi.GPIO as GPIO
import logging
import time
import apprise
import keypad
from tkinter import messagebox, ttk
from PIL import Image, ImageTk
from pirc522 import RFID

logo = "/home/pi/.Application/Images/E.L.S.S_logo.png"
main_color = '#347d79'
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

""" 
Function 0: Creates the necessary databases for the master console 
"""
def create_database():
    """
    Creation of Users.db
    """
    with sqlite3.connect(".Users.db") as db:
        cursor = db.cursor()

    cursor.execute('''
    CREATE TABLE IF NOT EXISTS users(
    uid INTEGER NOT NULL,
    firstname VARCHAR(20) NOT NULL,
    lastname VARCHAR(20) NOT NULL,
    status VARCHAR(3) NOT NULL,
    role VARCHAR(13) NOT NULL,
    labkeys VARCHAR(20) NOT NULL);
    ''')
    cursor.close()

    """
    Creation of Keys.db
    """
    with sqlite3.connect(".Keys.db") as db_thread:
        cursor = db_thread.cursor()
    cursor.execute('''
    CREATE TABLE IF NOT EXISTS keys(
    uid INTEGER NOT NULL,
    name VARCHAR(20) NOT NULL,
    status VARCHAR(10) NOT NULL);
    ''')
    cursor.close()
    
    """
    Creation of Code.db
    """
    with sqlite3.connect(".Code.db") as db_thread:
        cursor = db_thread.cursor()
    cursor.execute('''
    CREATE TABLE IF NOT EXISTS code(
    arming VARCHAR(4) NOT NULL,
    armed VARCHAR(5) NOT NULL);
    ''')
    
    check = '0000'
    cursor.execute("""
    SELECT COUNT (*) FROM code
    """)
    results = cursor.fetchall()
    if results[0][0]==0:
        cursor.execute("""
        INSERT INTO code(arming, armed)
        VALUES (?, ?)
        """,(check,'false'))
        db_thread.commit()
    cursor.close()

""" 
Function 1: Receives the UID input as well as the full name
            of the user and stores them in the database, after
            performing a validity check for the RFID tag
"""
def new_user(frame, UID, firstname, lastname, role):
    """
    Connect to Users.db
    """
    with sqlite3.connect(".Users.db") as db:
        cursor = db.cursor()

    if UID == 0 or len(firstname) == 0 or lastname == 0 or role==0:
        frame.destroy()
        messagebox.showerror("Error", "One or more fields are empty!!!!")
    else:
        cursor.execute("""
        SELECT *  FROM users WHERE uid = ?
        """, (UID, ))
        results = cursor.fetchall()
        if results:
            frame.destroy()
            messagebox.showerror("Add User", "The RFID tag is already in use!!")
            cursor.close()
        else:
            cursor.execute("""
            SELECT *  FROM users WHERE
            firstname = ? AND
            lastname = ? AND
            role = ?
            """, (firstname, lastname, role,))
            results = cursor.fetchall()
            if results:
                frame.destroy()
                messagebox.showerror("Add User", "User already has RFID")
                cursor.close()
            else:
                cursor.execute("""
                INSERT INTO users(uid, firstname, lastname, status, role, labkeys)
                VALUES (?, ?, ?, ?, ?, ?)
                """,(UID, firstname, lastname, 'No', role, 'None'))

                db.commit()
                frame.destroy()
                messagebox.showinfo("Add User", "New user added!!!")
                cursor.close()

""" 
Function 2: Receives the UID as input and deletes the user's
            credentials from the database
"""
def delete_user(frame, UID, name, surname, admin_UID):
    """
    Connect to Users.db
    """
    with sqlite3.connect(".Users.db") as db:
        cursor = db.cursor()

    if len(admin_UID)==0:
        messagebox.showerror("Remove user", "Unauthorized action!!")
    else:
        cursor.execute("""
        SELECT role  FROM users
        WHERE uid = ?
        """, (admin_UID,))
        results = cursor.fetchall()
        found = ''.join((results[0]))
        if found=="Administrator":
            if len(UID) == 0:
                if len(name)==len(surname)==0:
                    messagebox.showerror("Remove user", "Empty data!!")
                else:
                    cursor.execute("""
                    SELECT *  FROM users
                    WHERE firstname = ? AND lastname = ?
                    """, (name, surname, ))
                    results = cursor.fetchall()
                    if results:
                        cursor.execute("""
                        DELETE FROM users
                        WHERE firstname = ? AND lastname = ?
                         """, (name, surname, ))
                        
                        db.commit()
                        frame.destroy()
                        messagebox.showinfo("Remove user", "User deleted from database!!!")
                        cursor.close()
                    else:
                        frame.destroy()
                        messagebox.showwarning("Remove user", "Invalid data!!")
                        cursor.close()
            else:
                cursor.execute("""
                SELECT *  FROM users WHERE uid = ?
                """, (UID,))
                results = cursor.fetchall()
                if results:
                    cursor.execute("""
                    DELETE FROM users WHERE uid = ?
                    """, (UID,))
                    
                    db.commit()
                    frame.destroy()
                    messagebox.showinfo("Remove user", "User deleted from database!!!")
                    cursor.close()
                else:
                    frame.destroy()
                    messagebox.showwarning("Remove user", "UID not valid!")
                    cursor.close()
        else:
            frame.destroy()
            messagebox.showwarning("Remove user", "Unauthorized action!")

"""
Function 3: Receives the UID as input and checks if the person
            checking in, is permitted. It's a threaded function
            and it runs continuously in the background 
"""
def check_in(ser):
    received_UID = ""
    i = 0
    check_in_logger = setup_logger("Check In Logger", "Check In Log File.log")
    #Create Apprise instance and specify notification service
    apobj = apprise.Apprise()
    #Discord (for testing)
    apobj.add('discord://831545681247338529/jhpK0ezigWjLXp8dorxXegaF0wM4WhT0twBukHHPID7YKOcerh-00kIBomN8VPZTDJSA')
    #Mattermost (final product)
    #apobj.add('mmosts://mm.spacedot.gr:443/i61hqf8abb885ym97zhtnxy9sw')
    while True:
        #Wait for the Check in system to send the RFID key
        if i<5:
            data = ser.read()
            int_data = int.from_bytes(data, "big")
            received_UID+=str(int_data)
            i=i+1
        else:
            #Establish new connection to database
            with sqlite3.connect(".Users.db") as db_thread:
                conn = db_thread.cursor()
            #Search for the received UID
            conn.execute("""
            SELECT *  FROM users WHERE uid = ?
            """, (int(received_UID, 10),))
            results_UID = conn.fetchall()
            if results_UID:                
                #Search for check in status
                conn.execute("""
                SELECT firstname, lastname  FROM users WHERE uid = ? AND status = "No"
                """, (int(received_UID, 10),))
                results_status = conn.fetchall()
                # Checking in
                if results_status:
                    conn.execute("""
                    UPDATE users
                    SET status = "Yes"
                    WHERE uid = ?
                    """, (int(received_UID, 10),))
                    db_thread.commit()
                    # Split tuple for easier conversion
                    firstname, lastname = results_status[0]
                    #Create log entry
                    check_in_logger.info(": User " + firstname + " " + lastname + " checked in")
                    #Send message to Check In System
                    ser.write(chr(1).encode('latin1'))
                    #Displays a welcome message for 2 seconds
                    top = tk.Toplevel()
                    top.geometry('300x100')
                    top.title('Check In')
                    Message = tk.Label(top, text = 'Welcome ' + firstname + ' ' + lastname, padx = 10, pady = 10).pack()
                    top.after(2000, top.destroy)
                    #Send notification to Discord
                    local_time = time.ctime(time.time())
                    apobj.notify(
                        title='Check in System',
                        body=local_time + " --> user " + firstname + " " + lastname  + " checked in"
                    )
                    conn.execute("""
                    SELECT COUNT (status) FROM users
                    WHERE status = 'Yes'
                    """)
                    results_count = conn.fetchall()
                    people = results_count[0][0]
                    if people == 1:
                        apobj.notify(
                        title = 'Laboratory Status',
                        body = 'Lab open'
                    )
                        
                    time.sleep(3)
                    apobj.notify(
                        title = 'People in lab',
                        body = str(people)+ " people currently working in the lab"
                    )
                    conn.close()
                # Checking out
                else:
                    #Search for check in status
                    conn.execute("""
                    SELECT firstname, lastname  FROM users WHERE uid = ? AND status = "Yes"
                    """, (int(received_UID, 10),))
                    results_status = conn.fetchall()
                    # Split tuple for easier conversion
                    firstname, lastname = results_status[0]
                    conn.execute("""
                    UPDATE users
                    SET status = "No"
                    WHERE uid = ?
                    """, (int(received_UID, 10),))
                    db_thread.commit()
                    #Create log entry
                    check_in_logger.info(": User " + firstname + " " + lastname + " checked out")
                    #Send message to Check In System
                    ser.write(chr(1).encode('latin1'))
                    #Displays a goodbye message for 2 seconds
                    top = tk.Toplevel()
                    top.geometry('300x100')
                    top.title('Check In')
                    Message = tk.Label(top, text = 'Goodbye ' + firstname + ' ' + lastname, padx = 10, pady = 10).pack()
                    top.after(2000, top.destroy)
                    #Send notification to discord
                    local_time = time.ctime(time.time())
                    apobj.notify(
                        title='Check in System',
                        body=local_time + " --> user " + firstname + " " + lastname  + " checked out"
                    )
                    conn.execute("""
                    SELECT COUNT (status) FROM users
                    WHERE status = 'Yes'
                    """)
                    results_count = conn.fetchall()
                    people= results_count[0][0]
                    time.sleep(3)
                    apobj.notify(
                        title = 'People in lab',
                        body = str(people)+ " people currently working in the lab"
                    )
                    if people == 0:
                        time.sleep(1)
                        apobj.notify(
                            title = 'Laboratory Status',
                            body = 'Lab closed'
                        )
                    conn.close()
            else:
                conn.close()
                #Create log entry
                local_time = time.ctime(time.time())
                check_in_logger.error("Unauthorized attempt at " + local_time)
                #Send message to Check In System
                ser.write(chr(2).encode('latin1'))
                #Send Disord Notification
                local_time = time.ctime(time.time())
                apobj.notify(
                        title='WARNING!',
                        body="Unauthorized RFID tag detection at " + local_time
                )
            received_UID = ""
            i = 0

"""
Function 4: Handles the lab key allocation and responds to the 
            Key Tracking system with one of the following codes: 
            code 0: something is wrong
            code 1: the user is valid and can get a set of keys            
            code 2: the user is valid and cannot get a new set of keys
                    until he/she returns the set of keys already in his/her
                    possession
            It's a threaded function and it runs continuously in the 
            background
"""
def key_alloc(ser):
    user_UID = ""
    key_UID = ""
    i = 0
    key_logger = setup_logger("Key Logger", "Key Log File.log")
    while True:
        if i<5:
            data = ser.read()
            int_data = int.from_bytes(data, "big")
            user_UID+=str(int_data)
            i=i+1
        else:
            with sqlite3.connect(".Users.db") as db_thread:
                conn = db_thread.cursor()
            #Search if the user exists
            conn.execute("""
            SELECT firstname, lastname, labkeys FROM users WHERE uid = ?
            """, (int(user_UID, 10),))
            results_user = conn.fetchall()
            
            if results_user:
                firstname, lastname, labkeys = results_user[0]
                conn.close()
                # The user acquires a set of keys
                if labkeys=="None":
                    #Send message to Key Tracker System
                    ser.write(chr(1).encode('latin1'))
                    key_UID = convert(ser)
                    with sqlite3.connect(".Keys.db") as db_thread:
                        conn = db_thread.cursor()
                    conn.execute("""
                    SELECT status, name FROM keys WHERE uid = ?
                    """, (int(key_UID, 10),))
                    results_key = conn.fetchall()
                    status, keyname = results_key[0]
                    if results_key:
                        # The scanned RFID tag is valid
                        status, name = results_key[0]
                        if status=="Free":
                            # Update the status of the set of keys that were taken in Keys.db
                            conn.execute("""
                            UPDATE keys
                            SET status = "Taken"
                            WHERE uid = ?
                            """, (int(key_UID, 10),))
                            db_thread.commit()
                            conn.close()
                            
                            # Update Users.db with the name of the set of keys that were taken
                            with sqlite3.connect(".Users.db") as db_thread:
                                conn = db_thread.cursor()
                            conn.execute("""
                            UPDATE users
                            SET labkeys = ?
                            WHERE uid = ?
                            """, (name, int(user_UID, 10),))
                            db_thread.commit()
                            conn.close()
                            
                            #Keep Log File
                            key_logger.info(": User " + firstname + " " + lastname + " got " + keyname)
                            
                            #Send message to Key Tracker System
                            ser.write(chr(1).encode('latin1'))
                            user_UID = ""
                            key_UID = ""
                            i = 0
                        else:
                            #Send message to Key Tracker System
                            ser.write(chr(0).encode('latin1'))
                            user_UID = ""
                            key_UID = ""
                            i = 0
                    else:
                        #Invalid RFID tag for key
                        ser.write(chr(0).encode('latin1'))
                        user_UID = ""
                        key_UID = ""
                        i = 0
                #The user returns his/hers set of keys
                else:
                    #Send message to Key Tracker System
                    ser.write(chr(2).encode('latin1'))
                    key_UID = convert(ser)
                    with sqlite3.connect(".Keys.db") as db_thread:
                        conn = db_thread.cursor()
                    conn.execute("""
                    SELECT status, name FROM keys WHERE uid = ?
                    """, (int(key_UID, 10),))
                    results_status = conn.fetchall()
                    
                    # Valid key scan
                    if results_status:
                        conn.close()
                        # The scanned RFID tag is valid
                        status, keyname = results_status[0]

                        with sqlite3.connect(".Users.db") as db_thread:
                            conn = db_thread.cursor()
                        conn.execute("""
                        SELECT labkeys FROM users WHERE uid = ?
                        """, (int(user_UID, 10),))
                        results_labkeys = conn.fetchall()
                        labkeys = ''.join((results_labkeys[0]))
                        conn.close()
                        # The keys are returned
                        if labkeys==keyname:
                            ser.write(chr(2).encode('latin1'))
                            with sqlite3.connect(".Keys.db") as db_thread:
                                conn = db_thread.cursor()
                            conn.execute("""
                            UPDATE keys
                            SET status = ?
                            WHERE uid = ?
                            """, ('Free', int(key_UID, 10),))
                            db_thread.commit()
                            conn.close()
                            with sqlite3.connect(".Users.db") as db_thread:
                                conn = db_thread.cursor()
                            conn.execute("""
                            UPDATE users
                            SET labkeys = 'None'
                            WHERE uid = ?
                            """, (int(user_UID, 10),))
                            db_thread.commit()
                            conn.close()
                            
                            key_logger.info(": User " + firstname + " " + lastname + " returned " + keyname)
                            
                            user_UID = ""
                            key_UID = ""
                            i = 0
                        # Wrong keys scanned
                        else:
                            ser.write(chr(0).encode('latin1'))
                            user_UID = ""
                            key_UID = ""
                            i = 0
                    #Invalid key scan
                    else:
                        ser.write(chr(0).encode('latin1'))
                        user_UID = ""
                        key_UID = ""
                        i = 0
            else:
                # The user is invalid
                ser.write(chr(0).encode('latin1'))
                user_UID = ""
                key_UID = ""
                i = 0

""" 
Function 7: Receives the UID as input and replaces the user's
            RFID key from the database
"""
def replace_usertag(frame, firstname, lastname):
    """
    Connect to database
    """
    with sqlite3.connect(".Users.db") as db:
        cursor = db.cursor()

    if len(firstname) == 0 or len(lastname)==0:
        messagebox.showerror("Replace tag", "Empty fields!!!")
    else:
        cursor.execute("""
        SELECT *  FROM users WHERE firstname = ? AND lastname = ?
        """, (firstname, lastname,))
        results = cursor.fetchall()
        if results:
            frame = tk.Toplevel()
            frame.geometry("150x100")
            frame.configure(background = main_color)
            
            UID_label = ttk.Label(frame, text="UID", background=main_color)
            UID_entry = ttk.Entry(frame, show='*')

            UID_label.pack()
            UID_entry.pack()
            
            UID_btn = ttk.Button(frame, text="Get new UID", command=lambda: readUID(UID_entry, UID_btn))
            UID_btn.pack()
            
            replace_btn = ttk.Button(frame, text="Replace tag", command=lambda: update_data(frame, int(UID_entry.get()), firstname, lastname))
            replace_btn.pack()
        else:
            frame.destroy()
            messagebox.showwarning("Replace tag", "Not a valid user")

""" 
Function 8: Receives the UID as input and replaces the labkey's
            RFID key from the database
"""
def replace_keytag(frame, name):
    """
    Connect to database
    """
    with sqlite3.connect(".Keys.db") as db:
        cursor = db.cursor()

    if len(name) == 0:
        messagebox.showerror("Replace tag", "Empty field!!!")
    else:
        cursor.execute("""
        SELECT *  FROM keys WHERE name = ?
        """, (name,))
        results = cursor.fetchall()
        if results:
            frame = tk.Toplevel()
            frame.geometry("150x100")
            frame.configure(background = main_color)
            
            UID_label = ttk.Label(frame, text="UID", background=main_color)
            UID_entry = ttk.Entry(frame, show='*')

            UID_label.pack()
            UID_entry.pack()
            
            UID_btn = ttk.Button(frame, text="Get new UID", command=lambda: readUID(UID_entry, UID_btn))
            UID_btn.pack()
            
            replace_btn = ttk.Button(frame, text="Replace tag", command=lambda: update_data(frame, int(UID_entry.get()), firstname, lastname))
            replace_btn.pack()
        else:
            frame.destroy()
            messagebox.showwarning("Replace tag", "Not a valid key")

"""
Function 9: Shows which member or members have a set of lab keys
            in their possession
"""
def keys():
    with sqlite3.connect(".Users.db") as db:
        cursor = db.cursor()
    
    cursor.execute("""
    SELECT firstname, lastname  FROM users WHERE labkeys!="None"
    """)
    results = cursor.fetchall()
    if results:
        top = tk.Toplevel()
        top.geometry('300x100')
        top.title("Who's got keys?")
        Message = tk.Label(top, text = results).pack()
        top.after(6000, top.destroy)
    else:
        top = tk.Toplevel()
        top.geometry('300x100')
        top.title("Who's got keys?")
        Message = tk.Label(top, text = "Nobody has a set of keys").pack()
        top.after(4000, top.destroy)

""" 
Function 10: Receives the full name of the user as input and
             changes his/her user role
"""
def change_role(frame, firstname, lastname, role, admin_UID):
    with sqlite3.connect(".Users.db") as db:
        cursor = db.cursor()

    if len(admin_UID)==0:
        messagebox.showerror("Remove user", "Unauthorized action!!")
    else:
        cursor.execute("""
        SELECT role  FROM users
        WHERE uid = ?
        """, (admin_UID,))
        results = cursor.fetchall()
        found = ''.join((results[0]))
        if found=="Administrator":
            if len(firstname) == 0 or len(lastname)==0 or len(role)==0:
                messagebox.showerror("Change role", "Empty fields!!!")
            else:
                cursor.execute("""
                SELECT *  FROM users WHERE firstname = ? AND lastname = ?
                """, (firstname, lastname,))
                results = cursor.fetchall()
                if results:
                    cursor.execute("""
                    UPDATE users
                    SET role=?
                    WHERE firstname=? AND lastname=?
                    """, (role, firstname, lastname))
                    db.commit()
                    frame.destroy()
                    messagebox.showinfo("Change Role", "Role of " + firstname
                                        + lastname + " is updated")
                else:
                    frame.destroy()
                    messagebox.showwarning("Change Role", "Not a valid user")
        else:
            frame.destroy()
            messagebox.showwarning("Remove user", "Unauthorized action!")

""" 
Function 11: Add a set of keys in the database
"""
def add_key(frame, key_UID, key_name, admin_UID):
    with sqlite3.connect(".Users.db") as db:
        cursor = db.cursor()

    if len(admin_UID)==0:
        messagebox.showerror("Add keys", "Unauthorized action!!")
    else:
        cursor.execute("""
        SELECT role  FROM users
        WHERE uid = ?
        """, (admin_UID,))
        results = cursor.fetchall()
        found = ''.join((results[0][0]))
        if found=="Administrator":
            if len(key_UID) == 0 or len(key_name) == 0:
                frame.destroy()
                messagebox.showerror("Error", "One or more fields are empty!!!!")
            else:
                cursor.execute("""
                SELECT *  FROM users WHERE uid = ?
                """, (key_UID, ))
                results = cursor.fetchall()
                if results:
                    frame.destroy()
                    messagebox.showerror("Add key", "The RFID tag belongs to a user!!")
                    cursor.close()
                else:
                    # Close previous connection
                    cursor.close()
                    # Connect to database
                    with sqlite3.connect(".Keys.db") as db:
                        cursor = db.cursor()
                    cursor.execute("""
                    SELECT *  FROM keys WHERE uid = ?
                    """, (key_UID, ))
                    results = cursor.fetchall()
                    if results:
                        frame.destroy()
                        messagebox.showerror("Add key", "RFID already in use")
                        cursor.close()
                    else:
                        cursor.execute("""
                        SELECT COUNT(*) FROM keys
                        WHERE name=?
                        """,(key_name,))
                        results_count = cursor.fetchall()
                        if results_count[0][0]==0:
                            cursor.execute("""
                            INSERT INTO keys(uid, name, status)
                            VALUES (?, ?, ?)
                            """,(key_UID, key_name, 'Free'))

                            db.commit()
                            frame.destroy()
                            messagebox.showinfo("Add Key", "New set of keys added!!!")
                            cursor.close()
                        else:
                            frame.destroy
                            messagebox.showerror("Add Key", "Choose different name")
                            cursor.close()
        else:
            frame.destroy()
            messagebox.showwarning("Add Key", "Unauthorized action!")

""" 
Function 12: Receives the key UID as input and deletes the set of
             keys from the database
"""
def del_key(frame, key_UID, key_name, admin_UID):
    """
    Connect to Users.db
    """
    with sqlite3.connect(".Users.db") as db:
        cursor = db.cursor()
    
    if len(admin_UID)==0:
        messagebox.showerror("Remove labkeys", "Unauthorized action!!")
    else:
        cursor.execute("""
        SELECT role  FROM users
        WHERE uid = ?
        """, (admin_UID,))
        results = cursor.fetchall()
        found = ''.join((results[0]))
        if found=="Administrator":
            if len(key_UID) == 0:
                if len(key_name)==0:
                    messagebox.showerror("Remove labkeys", "Empty data!!")
                else:
                    cursor.close()
                    with sqlite3.connect(".Keys.db") as db:
                        cursor = db.cursor()
                    cursor.execute("""
                    SELECT *  FROM keys WHERE name = ?
                    """, (key_name,))
                    results = cursor.fetchall()
                    if results:
                        cursor.execute("""
                        DELETE FROM keys
                        WHERE name = ?
                         """, (key_name, ))
                        db.commit()
                        cursor.close()
                        frame.destroy()
                        messagebox.showinfo("Remove labkeys", "Key deleted from database!!!")
                        cursor.close()
                    else:
                        frame.destroy()
                        messagebox.showwarning("Remove labkeys", "Invalid data!!")
                        cursor.close()
            else:
                # Close previous connection
                cursor.close()
                with sqlite3.connect(".Keys.db") as db:
                    cursor = db.cursor()
                cursor.execute("""
                SELECT *  FROM keys WHERE uid = ?
                """, (key_UID,))
                results = cursor.fetchall()
                if results:
                    cursor.execute("""
                    DELETE FROM keys WHERE uid = ?
                    """, (key_UID,))
                    
                    db.commit()
                    frame.destroy()
                    messagebox.showinfo("Remove labkeys", "Key deleted from database!!!")
                    cursor.close()
                else:
                    frame.destroy()
                    messagebox.showwarning("Remove labkeys", "UID not valid!")
                    cursor.close()
        else:
            frame.destroy()
            messagebox.showwarning("Remove key", "Unauthorized action!")
        
#----------------------------------------------------------------------------------------------------------------------#
"""
Description: Insert data to database:
"""
def update_data(frame, new_UID, firstname, lastname):
    """
    Creation of database
    """
    with sqlite3.connect(".Users.db") as db:
        cursor = db.cursor()

    cursor.execute("""
    SELECT *  FROM users WHERE uid = ?
    """, (new_UID,))
    results = cursor.fetchall()
    if results:
        frame.destroy()
        messagebox.showerror("Remove user", "The RFID tag is already in use")
    else:
        cursor.execute("""
        UPDATE users
        SET uid=?
        WHERE firstname=? AND lastname=?
        """, (new_UID, firstname, lastname))
        db.commit()
        frame.destroy()
        messagebox.showinfo("Remove user", "New tag was given to " + firstname + " " + lastname)

"""
Description: Read UID from RFID Tag
"""
def readUID(entry, btn):
    GPIO.setwarnings(False)
    rdr = RFID()
    flag = True
    while flag:
        rdr.wait_for_tag()
        (error, tag_type) = rdr.request()
        if not error:
            (error, uid) = rdr.anticoll()
            if not error:
                UID = ''.join(str(x) for x in uid)
                entry.insert(0, str(UID))
                entry.config(state='disabled')
                btn.config(state='disabled')
                rdr.stop_crypto()
                flag = False
    rdr.cleanup()
                      
"""
Description: insert logo to the application
"""
def logo_insert(frame):
    load = Image.open(logo)
    render = ImageTk.PhotoImage(load.resize((150, 150)), Image.ANTIALIAS)
    img = tk.Label(frame, image=render, bg=main_color)
    img.image = render
    img.pack()
    
"""
Description: Convert incoming bytes to string
"""
def convert(port):
    string = ""
    i = 0
    while True:
        if i<5:
            data = port.read()
            int_data = int.from_bytes(data, "big")
            string+=str(int_data)
            i=i+1
        else:
            break
    return string

"""
Description: Create log files
"""
def setup_logger(name, log_file, level = logging.INFO):
    handler = logging.FileHandler(log_file)
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    return logger