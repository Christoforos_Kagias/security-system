import RPi.GPIO as GPIO
import time
import serial
import threading
import apprise
import sys
import sqlite3
from tkinter import messagebox


font1 = ("Arial", 11, "bold italic")
font2 = ("Times", 11, "bold")
font3 = ("Times", 18, "bold italic")
font4 = ("Times", 14)
button_font = ("Times", 15)
main_color = '#347d79'
#--------------------------------------------------------------------#

# Columns
C1 = 13 # or 27 in BCM pinout mode
C2 = 7  # or 4 in BCM pinout mode
C3 = 16 # or 23 in BCM pinout mode
#Rows
L1 = 11 # or 17 in BCM pinout mode
L2 = 15 # or 22 in BCM pinout mode
L3 = 29 # or 5 in BCM pinout mode
L4 = 31 # or 6 in BCM pinout mode

#set GPIO mode
def set_keypad():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)

    GPIO.setup(L1, GPIO.OUT)
    GPIO.setup(L2, GPIO.OUT)
    GPIO.setup(L3, GPIO.OUT)
    GPIO.setup(L4, GPIO.OUT)

    GPIO.setup(C1, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
    GPIO.setup(C2, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
    GPIO.setup(C3, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)


"""
Read input from keypad
"""
def readline(line, characters):
    value=""
    GPIO.output(line, GPIO.HIGH)
    if (GPIO.input(C1) == 1):
        value = characters[0]
    if (GPIO.input(C2) == 1):
        value = characters[1]
    if (GPIO.input(C3) == 1):
        value = characters[2]
    GPIO.output(line, GPIO.LOW)
    return value

"""
Description: Keypad handler
"""
def get_digit():
    char=""
    while True:
        char = readline(L1, ["1", "2", "3"])
        if (char!=""):
            return char
        char = readline(L4, ["4", "5", "6"])
        if (char!=""):
            return char
        char = readline(L3, ["7", "8", "9"])
        if (char!=""):
            return char
        char = readline(L2, ["*", "0", "#"])
        if (char!=""):
            return char
        time.sleep(0.15)

"""
Displays messages on screen depending on motion detection
"""
def motion_handler(port, flag):
    motion_flag = ""
    #Create Apprise instance and specify notification service
    apobj = apprise.Apprise()
    #Discord (for testing)
    apobj.add('discord://864430238376001537/SgFzEr-gQbSuXo31LQDn0Waqe-ihRVKnttkEmbS6Qb-aXw0Ly1nBsfWf4fSmlLeHCyuA')
    #Mattermost (final product)
    #apobj.add('mmosts://mm.spacedot.gr:443/mntboog97bg3xkshd5c3j74ihh')
    while flag:
        data = port.read()
        int_data = int.from_bytes(data, "big")
        motion_flag=str(int_data)
        if motion_flag=="0":
            #messagebox.showwarning("System", "Motion detected")
            local_time = time.ctime(time.time())
            apobj.notify(
                title='ALERT',
                body=local_time + " Motion detected"
            )
        else:
            motion=""

#--------------------------------------------------------------------#
"""
Set the arming code
"""            
def set_code(entry, btn):
    set_keypad()
    code=""
    i = 0
    while i<=3:
        text = get_digit()
        #If the user makes a mistake entering the code, the entry can be cleared by pressing "*"
        if text == "*":
            entry.delete(0, 'end')
            i = 0
        else:
            code += text
            entry.insert(i, text)
            i = i + 1
        time.sleep(0.2)
    entry.config(state='disabled')
    btn.config(state='disabled')
        
def authorize(frame, UID, code):
    """
    Creation of database
    """
    with sqlite3.connect(".Users.db") as db:
        cursor = db.cursor()
        
    cursor.execute("""
    SELECT *  FROM users WHERE
    uid=?
    """, (UID,))
    results = cursor.fetchall()
    if results:
        """
        Creation of database
        """
        with sqlite3.connect(".Code.db") as db:
            cursor = db.cursor()
            
        cursor.execute("""
        UPDATE code
        SET arming=?
        """, (code,))
        db.commit()
        frame.destroy()
        messagebox.showinfo("Set code", "Code Updated!")
        GPIO.cleanup()
    else:
        frame.destroy()
        messagebox.showerror("Change Code", "Unauthorized action")
        
#--------------------------------------------------------------------#

"""
Function 5: Activates the system after the correct arming code is entered
"""
def arm(frame1, frame2, frame3, label, entry, serial1, serial2):
    set_keypad()
    code=""
    motion1 = True
    motion2 = True
    i = 0
    while i<=3:
        text = get_digit()        
        #If the user makes a mistake entering the code, the entry can be cleared by pressing "*"
        if text == "*":
            entry.delete(0, 'end')
            i = 0
        else:
            code += text
            entry.insert(i, text)
            i = i + 1
        time.sleep(0.2)
    with sqlite3.connect(".Code.db") as db:
        cursor = db.cursor()
    cursor.execute("""
    SELECT *  FROM code WHERE
    arming = ?
    """, (code,))
    results = cursor.fetchall()
    if results:
        cursor.execute("""
        SELECT *  FROM code WHERE
        armed = 'false'
        """)
        results = cursor.fetchall()
        if results:
            cursor.execute("""
            UPDATE code
            SET armed='true'
            """
            )
            db.commit()
            cursor.close()
            frame1.destroy()
            frame2.configure(bg="#F21F1F")
            frame3.configure(bg="#F21F1F")
            label.config(bg="#F21F1F")
            #The system is armed after 30 seconds
            time.sleep(30)
            messagebox.showinfo("System info", "System armed")
            #First sensor
            serial1.write(bytes(b'1'))
            serial1_thread = threading.Thread(target = motion_handler, args=(serial1, motion1,))
            serial1_thread.daemon = True
            serial1_thread.start()
            #Second sensor
            serial2.write(bytes(b'1'))
            serial2_thread = threading.Thread(target = motion_handler, args=(serial2, motion2,))
            serial2_thread.daemon = True
            serial2_thread.start()
        else:
            frame1.destroy()
            messagebox.showwarning("System info", "System already armed")
    else:
        frame1.destroy()
        messagebox.showerror("System info", "Wrong arming code")
    GPIO.cleanup()


"""
Function 6: Deactivates the system after the correct disarming code is
            entered
"""
def disarm(frame1, frame2, frame3, label, entry, serial1, serial2):
    set_keypad()
    code=""
    i = 0
    while i<=3:
        text = get_digit()
        #If the user makes a mistake entering the code, the entry can be cleared by pressing "*"
        if text == "*":
            entry.delete(0, 'end')
            i = 0
        else:
            code += text
            entry.insert(i, text)
            i = i + 1
        time.sleep(0.2)
    with sqlite3.connect(".Code.db") as db:
        cursor = db.cursor()
    cursor.execute("""
    SELECT *  FROM code WHERE
    arming = ?
    """, (code,))
    results = cursor.fetchall()
    if results:
        cursor.execute("""
        SELECT *  FROM code WHERE
        armed = 'true'
        """)
        results = cursor.fetchall()
        if results:
            cursor.execute("""
            UPDATE code
            SET armed='false'
            """
            )
            db.commit()
            cursor.close()
            frame2.configure(bg=main_color)
            frame3.configure(bg=main_color)
            label.config(bg=main_color)
            serial1.write(bytes(b'0'))
            serial2.write(bytes(b'0'))
            messagebox.showinfo("System info", "System disarmed")
            frame1.destroy()
    else:
        frame1.destroy()
        messagebox.showerror("System info", "Wrong code")
    GPIO.cleanup()
