#!/usr/bin/python3

import tkinter as tk
import RPi.GPIO as GPIO
import serial
import threading
import time
import sqlite3
from PIL import Image, ImageTk
from pirc522 import RFID
from tkinter import ttk, messagebox
from tkinter.ttk import Entry

# Private imports
import functions
import ports
import keypad
import INA219

# Global variable declarations
main_color = '#347d79'
font1 = ("Arial", 11, "bold italic")
font2 = ("Times", 11, "bold")
font3 = ("Times", 18, "bold italic")
font4 = ("Times", 14)
button_font = ("Times", 15)

#Serial port declarations
ACM = ports.ACM_ports()
USB = ports.USB_ports()
check_in_ser = serial.Serial(ACM[0], 9600)
key_ser = serial.Serial(ACM[1], 9600)
motion1 = serial.Serial(USB[0], 9600)
motion2 = serial.Serial(USB[1], 9600)

######################---------GUI-----------#########################
"""
Add new user window
"""
class Add_User(tk.Toplevel):

    def __init__(self, master = None):
        super().__init__(master = master)
        
        """
        Window configuration: Title, size and background color
        """
        self.title("Add new user")
        self.geometry('350x250')
        self.configure(bg=main_color)
        self.resizable(0, 0)
        OPTIONS = ['--', 'Member', 'Administrator']

        # Use frames to organize the buttons, labels and entry boxes in the window
        frame1 = tk.Frame(self, bg = main_color)
        frame2 = tk.Frame(frame1, bg = main_color)
        frame3 = tk.Frame(frame2, bg = main_color)
        frame4 = tk.Frame(frame3, bg = main_color)
        
        frame4.pack(pady=5)
        frame3.pack(pady=5)
        frame2.pack(pady=5)
        frame1.pack(pady=5)

        """
        Button and label layout
        """
        UID_lbl = ttk.Label(frame1, text="UID", background=main_color, font=font2)
        UID_lbl.pack(side="left")
        UID_entry = ttk.Entry(frame1, show='*')
        UID_entry.pack(side="left")
        UID_btn = ttk.Button(frame1, text="Get UID", command=lambda: functions.readUID(UID_entry, UID_btn))
        UID_btn.pack(side="left", pady=5, padx=3)
        name_lbl = ttk.Label(frame2, text="First Name", background=main_color, font=font2)
        name_lbl.pack(side="left")
        name_entry = ttk.Entry(frame2)
        name_entry.pack(side="left")
        surname_lbl = ttk.Label(frame3, text="Last Name ", background=main_color, font=font2)
        surname_lbl.pack(side="left")
        surname_entry = ttk.Entry(frame3)
        surname_entry.pack(side="left")
        
        user_role = tk. StringVar(self)
        user_role.set(OPTIONS[0])
        role_lbl = ttk.Label(frame4, text="User Role", background=main_color, font=font2)
        role_lbl.pack(side="left")
        role_menu = ttk.OptionMenu(frame4, user_role, *OPTIONS)
        role_menu.pack(pady=5)

        add_btn = ttk.Button(self, text="Insert",
                             command=lambda: functions.new_user(self, UID_entry.get(), name_entry.get(), surname_entry.get(), user_role.get()))
        add_btn.pack(pady=5, padx=5)

"""
Delete user information window
"""
class Del_User(tk.Toplevel):

    def __init__(self, master = None):
        super().__init__(master = master)

        """
        Window configuration: Title, size and background color
        """
        self.title("Delete existing user's information")
        self.geometry('390x300')
        self.configure(bg=main_color)
        self.resizable(0, 0)

        # Use frames to organize the buttons, labels and entry boxes in the window
        frame1 = tk.Frame(self, bg = main_color)
        frame2 = tk.Frame(frame1, bg = main_color)
        frame3 = tk.Frame(frame2, bg = main_color)
        frame4 = tk.Frame(frame3, bg = main_color)
        
        frame4.pack(pady=5)
        frame3.pack(pady=5)
        frame2.pack(pady=5)
        frame1.pack(pady=5)

        """
        Button and label layout
        """
        instruction_lbl = ttk.Label(frame4, text="USE EITHER THE FULL NAME OR THE\n"
                                               "   RFID TAG TO DELETE THE USER\n", background=main_color, font=font1)
        instruction_lbl.pack()
        UID_lbl = ttk.Label(frame2, text="UID                  ", background=main_color, font=font2)
        UID_lbl.pack(side="left")
        UID_entry = ttk.Entry(frame2, show='*')
        UID_entry.pack(side="left")
        UID_btn = ttk.Button(frame2, text="Get UID", width = 15, command=lambda: functions.readUID(UID_entry, UID_btn))
        UID_btn.pack(pady=5, padx=5)
        
        #authorization
        admin_UID_lbl = ttk.Label(frame1, text="Admininstrator", background=main_color, font=font2)
        admin_UID_lbl.pack(side="left")
        admin_UID_entry = ttk.Entry(frame1, show='*')
        admin_UID_entry.pack(side="left")
        admin_UID_btn = ttk.Button(frame1, text="Get UID", width = 15, command=lambda: functions.readUID(admin_UID_entry, admin_UID_btn))
        admin_UID_btn.pack(pady=5, padx=5)
        
        space = tk.Label(self, text="\n", background=main_color)
        space.pack()
        name_lbl = ttk.Label(frame3, text="First Name", background=main_color, font=font2)
        name_lbl.pack(side="left")
        name_entry = ttk.Entry(frame3)
        name_entry.pack(side="left")
        surname_lbl = ttk.Label(frame4, text="Last Name ", background=main_color, font=font2)
        surname_lbl.pack(side="left")
        surname_entry = ttk.Entry(frame4)
        surname_entry.pack(side="left")
        
        del_btn = ttk.Button(self, text="Delete", width = 15,
                             command=lambda: functions.delete_user(self, UID_entry.get(), name_entry.get(), surname_entry.get(), admin_UID_entry.get()))
        del_btn.pack(pady=3)

"""
Assign new tag to existing user
"""
class Change_UserTag(tk.Toplevel):

    def __init__(self, master = None):
        super().__init__(master = master)

        """
        Window configuration: Title, size and background color
        """
        self.title("Replace User Tag")
        self.geometry('300x200')
        self.configure(bg=main_color)
        self.resizable(0, 0)

        # Use frames to organize the buttons, labels and entry boxes in the window
        frame1 = tk.Frame(self, bg = main_color)
        frame2 = tk.Frame(frame1, bg = main_color)
        
        frame2.pack(pady=5)
        frame1.pack(pady=5)

        """
        Button and label layout
        """
        name_lbl = ttk.Label(frame1, text="First Name", background=main_color, font=font2)
        surname_lbl = ttk.Label(frame2, text="Last Name ", background=main_color, font=font2)
        name_entry = ttk.Entry(frame1)
        surname_entry = ttk.Entry(frame2)
        
        name_lbl.pack(side="left")
        name_entry.pack(side="left")
        surname_lbl.pack(side="left")
        surname_entry.pack(side="left")
        
        replace_btn = ttk.Button(self, text="Assign new tag",
                                 command = lambda: functions.replace_usertag(self, name_entry.get(), surname_entry.get()))
        replace_btn.pack(pady=5)
        
"""
Assign new tag to existing user
"""
class Change_KeyTag(tk.Toplevel):

    def __init__(self, master = None):
        super().__init__(master = master)

        """
        Window configuration: Title, size and background color
        """
        self.title("Replace Key Tag")
        self.geometry('300x200')
        self.configure(bg=main_color)
        self.resizable(0, 0)

        # Use frames to organize the buttons, labels and entry boxes in the window
        frame1 = tk.Frame(self, bg = main_color)
        frame1.pack(pady=5)

        """
        Button and label layout
        """
        name_lbl = ttk.Label(frame1, text="Key Name", background=main_color, font=font2)
        name_entry = ttk.Entry(frame1)
        
        name_lbl.pack(side="left")
        name_entry.pack(side="left")
        
        replace_btn = ttk.Button(self, text="Assign new tag",
                                 command = lambda: functions.replace_keytag(self, name_entry.get()))
        replace_btn.pack(pady=5)

"""
Assign new role to existing user
"""
class Change_Role(tk.Toplevel):

    def __init__(self, master = None):
        super().__init__(master = master)

        """
        Window configuration: Title, size and background color
        """
        self.title("Change role")
        self.geometry('350x200')
        self.configure(bg=main_color)
        self.resizable(0, 0)

        # Use frames to organize the buttons, labels and entry boxes in the window
        frame1 = tk.Frame(self, bg = main_color)
        frame2 = tk.Frame(frame1, bg = main_color)
        frame3 = tk.Frame(frame2, bg = main_color)
        frame4 = tk.Frame(frame3, bg = main_color)
        
        frame4.pack(pady=5)
        frame3.pack(pady=5)
        frame2.pack(pady=5)
        frame1.pack(pady=5)
        
        OPTIONS = ['--', 'Member', 'Administrator']

        """
        Button and label layout
        """
        name_lbl = ttk.Label(frame3, text="First Name", background=main_color, font=font2)
        name_lbl.pack(side="left")
        name_entry = ttk.Entry(frame3)
        name_entry.pack(side="left")
        surname_lbl = ttk.Label(frame4, text="Last Name ", background=main_color, font=font2)
        surname_lbl.pack(side="left")
        surname_entry = ttk.Entry(frame4)
        surname_entry.pack(side="left")
        
        user_role = tk. StringVar(self)
        user_role.set(OPTIONS[0])
        role_lbl = ttk.Label(frame2, text="New user role", background=main_color, font=font2)
        role_lbl.pack(side="left")
        role_menu = ttk.OptionMenu(frame2, user_role, *OPTIONS)
        role_menu.pack(pady=5)
        
        #authorization
        admin_UID_lbl = ttk.Label(frame1, text="Admininstrator", background=main_color, font=font2)
        admin_UID_lbl.pack(side="left")
        admin_UID_entry = ttk.Entry(frame1, show='*')
        admin_UID_entry.pack(side="left")
        admin_UID_btn = ttk.Button(frame1, text="Get UID", width = 15, command=lambda: functions.readUID(admin_UID_entry, admin_UID_btn))
        admin_UID_btn.pack(pady=5, padx=5)
        
        new_role_btn = ttk.Button(self, text="Assign new role",
                                 command = lambda: functions.change_role(self, name_entry.get(), surname_entry.get(), user_role.get(), admin_UID_entry.get()))
        new_role_btn.pack(pady=5)

"""
Add a set of keys
"""
class Add_key(tk.Toplevel):

    def __init__(self, master = None):
        super().__init__(master = master)

        """
        Window configuration: Title, size and background color
        """
        self.title("Add a set of keys")
        self.geometry('390x300')
        self.configure(bg=main_color)
        self.resizable(0, 0)

        # Use frames to organize the buttons, labels and entry boxes in the window
        frame1 = tk.Frame(self, bg = main_color)
        frame2 = tk.Frame(frame1, bg = main_color)
        frame3 = tk.Frame(frame2, bg = main_color)
        frame4 = tk.Frame(frame3, bg = main_color)
        
        frame4.pack(pady=5)
        frame3.pack(pady=5)
        frame2.pack(pady=5)
        frame1.pack(pady=5)

        """
        Button and label layout
        """
        UID_lbl = ttk.Label(frame3, text="UID                  ", background=main_color, font=font2)
        UID_lbl.pack(side="left")
        UID_entry = ttk.Entry(frame3, show='*')
        UID_entry.pack(side="left")
        UID_btn = ttk.Button(frame3, text="Get UID", width = 15, command=lambda: functions.readUID(UID_entry, UID_btn))
        UID_btn.pack(pady=5, padx=5)
        
        #authorization
        admin_UID_lbl = ttk.Label(frame1, text="Admininstrator", background=main_color, font=font2)
        admin_UID_lbl.pack(side="left")
        admin_UID_entry = ttk.Entry(frame1, show='*')
        admin_UID_entry.pack(side="left")
        admin_UID_btn = ttk.Button(frame1, text="Get UID", width = 15, command=lambda: functions.readUID(admin_UID_entry, admin_UID_btn))
        admin_UID_btn.pack(pady=5, padx=5)
        
        space = tk.Label(self, text="\n", background=main_color)
        space.pack()
        name_lbl = ttk.Label(frame2, text="Key Name        ", background=main_color, font=font2)
        name_lbl.pack(side="left")
        name_entry = ttk.Entry(frame2)
        name_entry.pack(side="left")
        
        add_btn = ttk.Button(self, text="Add", width = 15,
                             command=lambda: functions.add_key(self, UID_entry.get(), name_entry.get(), admin_UID_entry.get()))
        add_btn.pack(pady=3)

"""
Delete set of keys
"""
class Del_key(tk.Toplevel):

    def __init__(self, master = None):
        super().__init__(master = master)

        """
        Window configuration: Title, size and background color
        """
        self.title("Remove a set of keys")
        self.geometry('390x300')
        self.configure(bg=main_color)
        self.resizable(0, 0)

        # Use frames to organize the buttons, labels and entry boxes in the window
        frame1 = tk.Frame(self, bg = main_color)
        frame2 = tk.Frame(frame1, bg = main_color)
        frame3 = tk.Frame(frame2, bg = main_color)
        frame4 = tk.Frame(frame3, bg = main_color)
        
        frame4.pack(pady=5)
        frame3.pack(pady=5)
        frame2.pack(pady=5)
        frame1.pack(pady=5)

        """
        Button and label layout
        """
        instruction_lbl = ttk.Label(frame4, text="USE EITHER THE NAME OR THE RFID TAG\n"
                                               "       TO DELETE THE SET OF KEYS\n", background=main_color, font=font1)
        instruction_lbl.pack()
        UID_lbl = ttk.Label(frame3, text="UID                  ", background=main_color, font=font2)
        UID_lbl.pack(side="left")
        UID_entry = ttk.Entry(frame3, show='*')
        UID_entry.pack(side="left")
        UID_btn = ttk.Button(frame3, text="Get UID", width = 15, command=lambda: functions.readUID(UID_entry, UID_btn))
        UID_btn.pack(pady=5, padx=5)
        
        #authorization
        admin_UID_lbl = ttk.Label(frame1, text="Admininstrator", background=main_color, font=font2)
        admin_UID_lbl.pack(side="left")
        admin_UID_entry = ttk.Entry(frame1, show='*')
        admin_UID_entry.pack(side="left")
        admin_UID_btn = ttk.Button(frame1, text="Get UID", width = 15, command=lambda: functions.readUID(admin_UID_entry, admin_UID_btn))
        admin_UID_btn.pack(pady=5, padx=5)
        
        space = tk.Label(self, text="\n", background=main_color)
        space.pack()
        name_lbl = ttk.Label(frame2, text="Key Name        ", background=main_color, font=font2)
        name_lbl.pack(side="left")
        name_entry = ttk.Entry(frame2)
        name_entry.pack(side="left")
        
        del_btn = ttk.Button(self, text="Delete", width = 15,
                             command=lambda: functions.del_key(self, UID_entry.get(), name_entry.get(), admin_UID_entry.get()))
        del_btn.pack(pady=3)

"""
System arming window
"""
class Arm(tk.Toplevel):

    def __init__(self, master = None):
        super().__init__(master = master)
        """
        Window configuration: Title, size and background color
        """
        self.title("Activate")
        self.geometry('300x100')
        self.configure(bg=main_color)
        self.resizable(0, 0)

        """
        Button and label layout
        """
        code_lbl = ttk.Label(self, text="Code", background=main_color, font=font2)
        code_entry = ttk.Entry(self, show='*')

        code_lbl.pack()
        code_entry.pack()
        
        arm_thread = threading.Thread(target = keypad.arm, args=(self, root, bottom_frame, new_line, code_entry, motion1, motion2,))
        arm_thread.daemon = True
        arm_thread.start()

"""
System disarming window
"""
class Disarm(tk.Toplevel):

    def __init__(self, master = None):
        super().__init__(master = master)

        """
        Window configuration: Title, size and background color
        """
        self.title("Disarm")
        self.geometry('300x100')
        self.configure(bg=main_color)
        self.resizable(0, 0)

        """
        Button and label layout
        """
        code_lbl = ttk.Label(self, text="Code", background=main_color, font=font2)
        code_entry = ttk.Entry(self, show='*')

        code_lbl.pack()
        code_entry.pack()
        
        disarm_thread = threading.Thread(target = keypad.disarm, args=(self, root, bottom_frame, new_line, code_entry, motion1, motion2,))
        disarm_thread.daemon = True
        disarm_thread.start()
        
"""
Set code window
"""
class Set_Code(tk.Toplevel):

    def __init__(self, master = None):
        super().__init__(master = master)

        """
        Window configuration: Title, size and background color
        """
        self.title("Set code")
        self.geometry('450x200')
        self.configure(bg=main_color)
        self.resizable(0, 0)

        # Use frames to organize the buttons, labels and entry boxes in the window
        frame1 = tk.Frame(self, bg = main_color)
        frame2 = tk.Frame(frame1, bg = main_color)
        frame3 = tk.Frame(frame2, bg = main_color)

        frame3.pack(pady=5)
        frame2.pack(pady=5)
        frame1.pack(pady=5)

        """
        Button and label layout
        """
        arm_lbl = ttk.Label(frame3, text="New Arming Code   ", background=main_color, font=font2)
        arm_lbl.pack(side="left")
        arm_entry = ttk.Entry(frame3, show='*')
        arm_entry.pack(side="left")
        arm_entry_btn = ttk.Button(frame3, text="Get new code", width = 15, command=lambda: keypad.set_code(arm_entry, arm_entry_btn))
        arm_entry_btn.pack(pady=5, padx=5)
        
        #Authorization
        admin_UID_lbl = ttk.Label(frame2, text="Admininstrator        ", background=main_color, font=font2)
        admin_UID_lbl.pack(side="left")
        admin_UID_entry = ttk.Entry(frame2, show='*')
        admin_UID_entry.pack(side="left")
        admin_UID_btn = ttk.Button(frame2, text="Get UID", width = 15, command=lambda: functions.readUID(admin_UID_entry, admin_UID_btn))
        admin_UID_btn.pack(pady=5, padx=5)
        
        new_code_btn = ttk.Button(frame1, text="Set code", width = 15, command=lambda: keypad.authorize(self, admin_UID_entry.get(), arm_entry.get()))
        new_code_btn.pack(pady=5, padx=5)

######################################################################
"""
Main window configuration: Title, frames, size and background color
"""

root = tk.Tk()
root.attributes('-zoomed', True)
root.title("E.L.S.S - Electonics Laboratory Security System")

bottom_frame = tk.Frame(root, bg = main_color)
bottom_frame.pack(side="bottom")
with sqlite3.connect(".Code.db") as db:
    cursor = db.cursor()
cursor.execute("""
SELECT *  FROM code WHERE
armed = 'false'
""")
results = cursor.fetchall()
if results:
    root.configure(bg=main_color)
    bottom_frame.configure(bg=main_color)
    new_line = tk.Label(bottom_frame, text="\n\n\n\n\n\n\n\n\n\n\n\n", bg = main_color)
else:
    root.configure(bg="#F21F1F")
    bottom_frame.configure(bg="#F21F1F")
    new_line = tk.Label(bottom_frame, text="\n\n\n\n\n\n\n\n\n\n\n\n", bg = "#F21F1F")

"""
Main window layout (buttons and labels)
"""

menubar = tk.Menu(root)
filemenu1 = tk.Menu(menubar, tearoff=0)
filemenu1.add_command(label="Add new user", command = lambda: Add_User(root))
filemenu1.add_command(label="Delete user", command = lambda: Del_User(root))
filemenu1.add_command(label="Replace user's RFID tag", command = lambda: Change_UserTag(root))
filemenu1.add_command(label="Change user Role", command = lambda: Change_Role(root))
filemenu1.add_separator()
filemenu1.add_command(label="Change Code", command = lambda: Set_Code(root))
menubar.add_cascade(label="System Manager", menu = filemenu1)

filemenu2 = tk.Menu(menubar, tearoff=0)
filemenu2.add_command(label="Add a set of keys", command = lambda: Add_key(root))
filemenu2.add_command(label="Remove a set of keys", command = lambda: Del_key(root))
filemenu2.add_command(label="Replace key's RFID tag", command = lambda: Change_KeyTag(root))
filemenu2.add_command(label="Who's got keys?", command = lambda: functions.keys())
menubar.add_cascade(label="Labkeys Manager", menu = filemenu2)

arm_btn = tk.Button(bottom_frame, text="Activate system", width = 20, height = 5, font = button_font,
                 bg = '#FFFFFF', activebackground = '#FFFF00')
arm_btn.bind("<Button-1>", lambda e: Arm(root))

disarm_btn = tk.Button(bottom_frame, text="Deactivate system", width = 20, height = 5, font = button_font,
                bg = '#FFFFFF', activebackground = '#FFFF00')
disarm_btn.bind("<Button-1>", lambda e: Disarm(root))

functions.logo_insert(root)

arm_btn.pack(side = "left")
disarm_btn.pack(side = "left")
new_line.pack()

"""
Thread to handle communication with the Check In System
"""
check_in_thread = threading.Thread(target = functions.check_in, args=(check_in_ser,))
check_in_thread.daemon = True
check_in_thread.start()

"""
Thread to handle communication with the Key Tracker System
"""
labkeys_thread = threading.Thread(target = functions.key_alloc, args=(key_ser,))
labkeys_thread.daemon = True
labkeys_thread.start()

"""
Thread to handle power outage and safe shutdown of the system
"""
UPS_thread = threading.Thread(target = INA219.UPS, args=())
UPS_thread.daemon = True
UPS_thread.start()

functions.create_database()

root.config(menu=menubar)
root.mainloop()
