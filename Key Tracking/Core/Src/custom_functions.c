/*
 * General_Functions.c
 *
 *   Created on: Mar 10, 2021
 *   Author: Christofer Kagias
 */

#include "custom_functions.h"
#include "MFRC522 RFID Reader.h"
#include "stm32f1xx.h"
#include "stdlib.h"
#include "usbd_cdc_if.h"

/**************************************DO NOT CHANGE ANYTHING BEYOND THIS LINE******************************************************/
/*
 * Color combinations for set_rgb function
 * (255, 0, 0);					blue
 * (0, 255, 0);					yellow
 * (0, 0, 255);					pink
 * (255, 255, 0);				green
 * (0, 255, 255);				red
 * (0, 0, 0);					white (close enough)
 * (255, 255, 255);				off
 */

/*
 * Timer declarations
 */
TIM_HandleTypeDef htim2;			// LED
TIM_HandleTypeDef htim3;			// Buzzer
TIM_HandleTypeDef htim4;			// Servo

/*
 * Variable declarations
 */

// RFID specific variables
uint8_t passwd[] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};		// Default RFID Tag password to access data
uint8_t status;											// Indicates if accessing the card data was successful
uint8_t authenticate;									// Indicates if the card data is valid
uint8_t str[MAX_LEN]; 									// Max_LEN = 16
uint8_t UID[5];											// RFID UID
uint8_t recv_flag[1];									// Buffer for USB data reception

/*
 * Void function that reads the UID of the user's MIFARE 1k tag through the RC522 RFID Reader
 * Arguments: none
 */
void read_userUID()
{
	status = MFRC522_Request(PICC_REQIDL, str);	// Read tag UID
	status = MFRC522_Anticoll(str);// Show tag UID
	if (status == MI_OK)
	{
		memcpy(UID, str, 5);
		check_userUID(UID[0], UID[1], UID[2], UID[3], UID[4]);
	}
	MFRC522_Init();
}

/*
 * Void function that reads the UID of the key's MIFARE 1k tag through the RC522 RFID Reader
 * Arguments: none
 */
void read_keyUID(uint8_t userflag)
{
	MFRC522_Init();
	uint8_t uflag = userflag;
	while(1)
	{
		status = MFRC522_Request(PICC_REQIDL, str);	// Read tag UID
		status = MFRC522_Anticoll(str);				// Show tag UID
		if (status == MI_OK)
		{
			break;
		}
	}
	memcpy(UID, str, 5);
	check_keyUID(UID[0], UID[1], UID[2], UID[3], UID[4], uflag);
	MFRC522_Init();
}

/*
 * This function is used to send the user UID to the master console and receive a response depending on the result of a SQL search query performed by the console
 * Arguments: 5 uint8_t values that correspond to a part of the UID of the MIFARE 1k tag
 *
 * The MCU transmits the 5 8-bit values to the master console and after a comparison with the existing database, receives through
 * serial communication a value that corresponds to the result of the database search
 *
 * 0 -> UID is not valid	-> the user is not valid --------------------------------------------------------------> blink red
 * 1 -> UID is valid 		-> the user is valid     --------------------------------------------------------------> blink green
 * 2 -> Special case 		-> the user is valid but must return his/her key before getting another set of keys ---> blink yellow
 *
 */
void check_userUID(uint8_t bitset1, uint8_t bitset2, uint8_t bitset3, uint8_t bitset4, uint8_t bitset5)
{
	CDC_Transmit_FS(&bitset1, 1);
	HAL_Delay(10);
	CDC_Transmit_FS(&bitset2, 1);
	HAL_Delay(10);
	CDC_Transmit_FS(&bitset3, 1);
	HAL_Delay(10);
	CDC_Transmit_FS(&bitset4, 1);
	HAL_Delay(10);
	CDC_Transmit_FS(&bitset5, 1);
	HAL_Delay(10);

	if (recv_flag[0] == 0)					// User is invalid
	{
		blink(0, 255, 255);
		buzzer(0, 150);
		HAL_Delay(2000);
	}

	else if (recv_flag[0] == 1)				// User is valid and without a set of keys
	{
		blink(255, 255, 0);
		buzzer(0, 75);
		HAL_Delay(1000);
		uint8_t user = recv_flag[0];
		htim4.Instance->CCR1 = 65;			// Unlock the door
		read_keyUID(user);
	}
	else if (recv_flag[0] == 2)				// User is valid but has a set of keys
	{
		blink(0, 255, 0);
		buzzer(0, 500);
		HAL_Delay(1000);
		uint8_t user = recv_flag[0];
		read_keyUID(user);
	}
}

/*
 * This function is used to send the key UID to the master console and receive a response depending on the result of a SQL search query performed by the console
 * Arguments: 5 uint8_t values that correspond to a part of the UID of the MIFARE 1k tag
 *
 * The MCU transmits the 5 8-bit values to the master console and after a comparison with the existing database, receives through
 * serial communication a value that corresponds to the result of the database search
 *
 * 0 -> UID is not valid 	-> the key is not valid --------------------------------------------------------------> blink red
 * 1 -> UID is valid 		-> the key is valid     --------------------------------------------------------------> blink green
 * 2 -> UID is not valid 	-> the key is returned by the user ---------------------------------------------------> blink yellow
 *
 */
void check_keyUID(uint8_t bitset1, uint8_t bitset2, uint8_t bitset3, uint8_t bitset4, uint8_t bitset5, uint8_t userflag)
{
	CDC_Transmit_FS(&bitset1, 1);
	HAL_Delay(10);
	CDC_Transmit_FS(&bitset2, 1);
	HAL_Delay(10);
	CDC_Transmit_FS(&bitset3, 1);
	HAL_Delay(10);
	CDC_Transmit_FS(&bitset4, 1);
	HAL_Delay(10);
	CDC_Transmit_FS(&bitset5, 1);
	HAL_Delay(10);

	// The scanned RFID tag is not registered to one of the keys or if the user attempts to take a second set of keys
	if (recv_flag[0] == 0)
	{
		blink(0, 255, 255);
		buzzer(0, 75);
		htim4.Instance->CCR1 = 15;			// Lock the door
		HAL_Delay(1000);
		blink(0, 0, 0);
	}

	// The scanned RFID tag is registered to one of the keys and the user is valid and does not have a set of keys
	else if ((recv_flag[0] == 1)&&(userflag==1))
	{
		blink(255, 255, 0);
		buzzer(0, 75);
		HAL_Delay(10000);
		htim4.Instance->CCR1 = 15;			// Lock the door
		HAL_Delay(1000);
		blink(0, 0, 0);
	}

	// The scanned RFID tag is registered to one of the keys and the user returns the key
	else
	{
		blink(0, 255, 0);
		buzzer(0, 500);
		htim4.Instance->CCR1 = 65;			// Unlock the door
		HAL_Delay(10000);
		htim4.Instance->CCR1 = 15;			// Lock the door
		HAL_Delay(1000);
		blink(0, 0, 0);
	}
}

/*
 * Void function that handles the external common anode RGB LED with the use of PWM.
 * Arguments: 3 uint32_t values from 0 to 255 (three 255 values turn the LED off)
 */
void set_rgb(uint32_t color1, uint32_t color2, uint32_t color3)
{
	htim2.Instance->CCR1 = color1;
	htim2.Instance->CCR2 = color2;
	htim2.Instance->CCR3 = color3;
}

/*
 * Void function that blinks the external common anode RGB (uses set_rgb() function)
 * Arguments: 3 uint32_t values from 0 to 255 (three 255 values turn the LED off)
 */
void blink(uint32_t color1, uint32_t color2, uint32_t color3)
{
	for (int i = 0; i<3; i++)
	{
		set_rgb(color1, color2, color3);
		HAL_Delay(100);
		set_rgb(255, 255, 255);
		HAL_Delay(100);
	}
}

/*
 * Void function that controls the buzzer
 * Arguments: tone and delay (indicates correct or incorrect output)
 */
void buzzer(uint32_t tone, uint32_t delay)
{
	for (int i = 0; i<3; i++)
	{
		htim3.Instance->CCR1 = tone;
		HAL_Delay(delay);
		htim3.Instance->CCR1 = 255;
		HAL_Delay(delay);
	}
	htim3.Instance->CCR1 = 255;
}
