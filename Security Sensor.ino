#define SensorPin 2

char activate = '0';
boolean armed = false;

// the setup routine runs once when you press reset:
void setup()
{
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  pinMode(SensorPin, INPUT);
//  pinMode(LED_BUILTIN, OUTPUT);
}


// the loop routine runs over and over again forever:
void loop()
{
  // Standby mode
  if (armed == false)
  {
    delay(1000);
  }
  else
  {
    int sensorvalue = digitalRead(SensorPin);
    if (sensorvalue == HIGH)
    {
      Serial.write(0);
//      digitalWrite(LED_BUILTIN, HIGH);
      delay(2000);
    }
    else
    {
//      digitalWrite(LED_BUILTIN, LOW);
    }
    delay(500);
  }
}


/*
  SerialEvent occurs whenever a new data comes in the hardware serial RX. This
  routine is run between each time loop() runs, so using delay inside loop can
  delay response. Multiple bytes of data may be available.
*/
void serialEvent() {
  while (Serial.available())
  {
    activate = (char)Serial.read();
    if (activate == '0') 
    {
      armed = false;
//      digitalWrite(LED_BUILTIN, LOW);
    }
    else if (activate == '1')
    {
      armed = true;
    }
  }
}
